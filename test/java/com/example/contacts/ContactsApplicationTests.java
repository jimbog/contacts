package com.example.contacts;

import com.example.contacts.services.ContactService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = ContactsApplication.class)
@AutoConfigureMockMvc
public class ContactsApplicationTests {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void testIndex() throws Exception {
        this.mockMvc.perform(get("/contacts/city/chicago"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testCreate() throws Exception {
        // No contact sent in payload
        this.mockMvc.perform(post("/contacts"))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testDelete() throws Exception {
        this.mockMvc.perform(delete("/contacts/1"))
                .andExpect(status().isNotFound());
    }
}
