# Contacts App
A contact management app created with Spring boot.


Assumptions
----------
*  The `date` field is a Java `Date`, the `address` field is described in is own class `Address`, all the other fields
are of class `String`.
* The class `Address` (which is embedded in the `Contact` model) has the following fields:
    * street
    * city
    * state
    * zip (a `String` since it may include non-digit characters) 

* The storage is a H2 in-memory db

Deployment
----------
The app can be packaged as a jar that can be deployed to aws, azure, pcf or other cloud service.
A packaged snapshot is included in the build directory of this repo.


Endpoints
---------
The endpoints for the basic REST interface are as follows:

* GET `/contacts` retrieve all of the contacts
* GET `/contacts/id` retrieve the contact by the specified `id`
* POST `/contacts` with payload will  add a new contact. The date should be in a Java parseable format i.e. `yyyy-MM-dd`
* PUT `/contacts/id` with payload will update the given the contact with the given `id`
* DELETE `/contacts/id` will delete the given the contact with the given `id`

The endpoints to search for a contact by phone or email:

* GET `/contacts?phone=555` retrieve a contact where either the personal or work phone match the phone query param
* GET `/contacts?email=name@example.org` retrieve a contact where the email matches the email query param

The endpoints to search for contacts of the same city or state:

* GET `/contacts/city/:cityName` retrieve all contacts where the city matches the `:cityName`
* GET `/contacts/state/:stateName` retrieve all contacts where the state matches the `:stateName`


### Endpoints Tests

* The GET `/contacts/city/:cityName` endpoint is tested for response OK and content-type
* The DELETE `/contacts/:invalidId` is tested to deliver a not-found response when requesting an ID that doesn't exist
* The POST `/contacts/` is tested to deliver a bad request for a request without a payload


#### An easy way to check the endpoints with "httpie"

* `http :8080/contacts                                                                                `
* `echo '{"name":"a", "address": {"city":"chicago", "state":"ca"}}' | http POST :8080/contacts        `
* `echo '{"name":"b", "address": {"city":"los angeles", "state":"ca"}}' | http POST :8080/contacts    `
* `echo '{"name":"a", "address": {"city":"posen", "state":"il"}, "birthdate":"1990-12-31"}' | http POST :8080/contacts      `
* `http :8080/contacts/state/il                                                                       `
* `http PUT :8080/contacts/2 name=magic                                                               `
