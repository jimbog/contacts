package com.example.contacts.controllers;

import com.example.contacts.models.Contact;
import com.example.contacts.services.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/contacts")
public class ContactsController {

    private ContactService contactService;

    @Autowired

    public ContactsController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping
    ResponseEntity<Object> index(){
        return ResponseEntity.ok(contactService.findAll());
    }

    // Retrieve​ ​ a ​ ​ contact​ ​ record
    @GetMapping(path = "/{id}")
    ResponseEntity<Object> show(@PathVariable Long id){
        Optional<Contact> contact = contactService.findById(id);

        return contact.isPresent() ? ResponseEntity.ok(contact) : ResponseEntity.notFound().build();
    }

    // Create​ ​ a ​ ​ contact​ ​ record
    @PostMapping
    ResponseEntity<Object> create(@RequestBody Contact contact){
        Contact savedContact = contactService.save(contact);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedContact.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    // Update​ ​ a ​ ​ contact​ ​ record
    @PutMapping(path = "/{id}")
    ResponseEntity<Object> update(@PathVariable long id, @RequestBody Contact contact){
        if (contactService.existsById(id)){
            contact.setId(id);
            contactService.save(contact);
            return ResponseEntity.ok(contact);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    // Delete​ ​ a ​ ​ contact​ ​ record
    @DeleteMapping("/{id}")
    ResponseEntity<Object> delete(@PathVariable long id){
        if (contactService.existsById(id)){
            contactService.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // Search​ ​ for​ ​ a ​ ​ record​ ​ by​ ​ email​ ​ or​ ​ phone​ ​ number
    @RequestMapping(params = "email")
    Contact findByEmail(@RequestParam("email") String email){
        return contactService.findByEmail(email);
    }

    // Search​ ​ for​ ​ a ​ ​ record​ ​ by​ ​phone​ ​ number
    @RequestMapping(params = "phone")
    Contact findByPhone(@RequestParam("phone") String phone){
        return contactService.findByPhone(phone);
    }

    // Retrieve​ ​ all​ ​ records​ ​ from​ ​ the​ ​ same​ ​ city
    @GetMapping(path = "/city/{city}")
    ResponseEntity<Object> findAllByCity(@PathVariable String city){
        return ResponseEntity.ok(contactService.findAllByCity(city));
    }

    // Retrieve​ ​ all​ ​ records​ ​ from​ ​ the​ ​ same​ ​ state​
    @GetMapping(path = "/state/{state}")
    ResponseEntity<Object> findAllByState(@PathVariable String state){
        return ResponseEntity.ok(contactService.findAllByState(state));
    }



}
