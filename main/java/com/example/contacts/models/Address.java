package com.example.contacts.models;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
class Address {
    private String street;
    private String city;
    private String state;
    private String zip;
}
