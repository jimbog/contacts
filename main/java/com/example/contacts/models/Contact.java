package com.example.contacts.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Contact {
    @Id
    @GeneratedValue
    private long id;

    private String name;
    private String company;
    private String email;
    private Date birthdate;
    private String workPhone;
    private String personalPhone;
    private Address address;
}
