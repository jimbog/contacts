package com.example.contacts.repositories;

import com.example.contacts.models.Contact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {
    Contact findContactByEmail(String email);
    Contact findContactByWorkPhoneOrPersonalPhone(String workPhone, String personalPhone);
    List<Contact> findByAddressCity(String city);
    List<Contact> findByAddressState(String state);

}
