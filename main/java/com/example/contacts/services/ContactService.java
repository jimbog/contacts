package com.example.contacts.services;

import com.example.contacts.models.Contact;
import com.example.contacts.repositories.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactService {
    private ContactRepository contactRepository;

    @Autowired
    public ContactService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    public Iterable<Contact> findAll(){
        return  this.contactRepository.findAll();
    }

    public Iterable<Contact> findAllByCity(String city){
        return this.contactRepository.findByAddressCity(city);
    }

    public Iterable<Contact> findAllByState(String state){
        return this.contactRepository.findByAddressState(state);
    }


    public Optional<Contact> findById(Long id){
        return  this.contactRepository.findById(id);
    }

    public boolean existsById(Long id){
        return  this.contactRepository.existsById(id);
    }


    public Contact save(Contact contact){
        return  this.contactRepository.save(contact);
    }

    public void deleteById(Long id){
        this.contactRepository.deleteById(id);
    }

    public Contact findByEmail(String email){
        return  this.contactRepository.findContactByEmail(email);
    }

    public Contact findByPhone(String phone){
        return  this.contactRepository.findContactByWorkPhoneOrPersonalPhone(phone, phone);
    }


}
